import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Local } from 'src/app/modelo/comercio/local.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LocalService } from 'src/app/modelo/comercio/local.service';


@Component({
  selector: 'app-local-dialogo',
  templateUrl: './local-dialogo.component.html',
  styleUrls: ['./local-dialogo.component.scss']
})
export class LocalDialogoComponent implements OnInit {

  form: FormGroup
  local: Local
  constructor(@Inject(MAT_DIALOG_DATA) data, private localServicio: LocalService, 
    private dialogo: MatDialogRef<LocalDialogoComponent>) { 
    this.local = data.local

   }

  ngOnInit() {
    //this.form = Local.getForm(new  Local())
    this.form = Local.getForm(this.local)
  }

  aceptar(){
    console.log(this.form.value)
    this.localServicio.createOrUpdate(this.form.value).subscribe( //Verifica con el id si esta guardado o no
      resultado => {
        //console.log(resultado)
        this.dialogo.close(resultado) //Cuando se cierre tenenmos que interceptarlo en la vista donde actualizara el listado
      }
    )
  }
  cancelar(){
    this.dialogo.close(null)
  }

}
