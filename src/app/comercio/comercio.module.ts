import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalesComponent } from './locales/locales.component';
import { SharedModule } from '../shared/shared.module';
import { LocalDialogoComponent } from './locales/local-dialogo/local-dialogo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilModule } from '../util/util.module';
import { UtilConfirmacionComponent } from '../shared/util-confirmacion/util-confirmacion.component';
import { SeccionesComponent } from './secciones/secciones.component';




@NgModule({
  declarations: [LocalesComponent, LocalDialogoComponent, SeccionesComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule, 
    ReactiveFormsModule, 
    UtilModule
  ],
  exports: [
    LocalesComponent,
    SeccionesComponent
  ],
  entryComponents: [
    UtilConfirmacionComponent,
    LocalDialogoComponent
  ]
})
export class ComercioModule { }
