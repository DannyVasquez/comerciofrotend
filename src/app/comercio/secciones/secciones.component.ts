import { Component, OnInit } from '@angular/core';
import { Seccion } from 'src/app/modelo/comercio/seccion.model';
import { SeccionService } from 'src/app/modelo/comercio/seccion.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UtilConfirmacionComponent } from 'src/app/shared/util-confirmacion/util-confirmacion.component';

@Component({
  selector: 'app-secciones',
  templateUrl: './secciones.component.html',
  styleUrls: ['./secciones.component.scss']
})
export class SeccionesComponent implements OnInit {
  
  secciones: Seccion[]
  seccion: Seccion

  constructor(private seccionesService: SeccionService) { }

  ngOnInit() {
    
    //this.cargarSecciones()
    //console.log(this.cargarSecciones())
  }
//consultas siempre en el padre
   /* 
  cargarSecciones() {
    this.seccionesService.list().subscribe(
      resultado2 => {
        this.secciones = resultado2
      }
    )
  }
  */



}
