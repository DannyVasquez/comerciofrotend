import { FormGroup, FormControl, Validators } from '@angular/forms'

export class Local {
    id: number
    nombre: string

    static getForm(local?: Local){ //Presentarme sin que tenga parametros para llenar
                                    //? es opcional 
        return new FormGroup({
            id: new FormControl(local.id),
            nombre: new FormControl(local.nombre, {validators: Validators.required}),

        });                            

    }
}

//Los controles son los atributos que quiero para mi formulario
//Para llamar este formulario se lo hace desde local-dialogo.component.ts