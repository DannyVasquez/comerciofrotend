import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class SeccionService extends BaseService{

  apiURL = 'http://localhost:8000/api/comercio/seccion/'
  
}
  