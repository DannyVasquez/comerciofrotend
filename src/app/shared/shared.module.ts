import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarraComponent } from './barra/barra.component';
import { UtilModule } from '../util/util.module';
import { UtilLocalComponent } from './util-local/util-local.component';
import { UtilConfirmacionComponent } from './util-confirmacion/util-confirmacion.component';

import { UtilSeccionComponent } from './util-seccion/util-seccion.component';
import { UtilSeccionItemComponent } from './util-seccion/util-seccion-item/util-seccion-item.component';
import { SeccionDialogoComponent } from './util-seccion/seccion-dialogo/seccion-dialogo.component';




@NgModule({
  declarations: [BarraComponent, UtilLocalComponent, UtilConfirmacionComponent, UtilSeccionComponent, UtilSeccionItemComponent,  SeccionDialogoComponent],
  imports: [
    CommonModule,
    UtilModule

  ],
  exports: [
    BarraComponent,
    UtilConfirmacionComponent,
    UtilLocalComponent,
    UtilSeccionComponent,
    UtilSeccionItemComponent,
  ],
  entryComponents: [
    UtilConfirmacionComponent,
    SeccionDialogoComponent
  ]
})
export class SharedModule { }
