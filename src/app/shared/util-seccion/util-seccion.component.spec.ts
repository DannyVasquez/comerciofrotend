import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilSeccionComponent } from './util-seccion.component';

describe('UtilSeccionComponent', () => {
  let component: UtilSeccionComponent;
  let fixture: ComponentFixture<UtilSeccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilSeccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilSeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
