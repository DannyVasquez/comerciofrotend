import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { Seccion } from 'src/app/modelo/comercio/seccion.model';
import { SeccionService } from 'src/app/modelo/comercio/seccion.service';
import { Local } from 'src/app/modelo/comercio/local.model';

@Component({
  selector: 'app-seccion-dialogo',
  templateUrl: './seccion-dialogo.component.html',
  styleUrls: ['./seccion-dialogo.component.scss']
})
export class SeccionDialogoComponent implements OnInit {
  formSeccion: FormGroup
  seccion: Seccion
  id_Local: Local
  constructor(@Inject(MAT_DIALOG_DATA) data, private seccionServicio: SeccionService, 
    private dialogo: MatDialogRef<SeccionDialogoComponent>) { 
      this.seccion = data.seccion,
      this.id_Local = data.id_Local //SE RECIBE EL ID DEL LOCAL

  }

  ngOnInit() {
    this.seccion.local = this.id_Local ///AQUI SE MANDA EL ID DEL LOCAL PARA CREARLO O PARA EDITAR
    this.formSeccion = Seccion.getForm(this.seccion)
    //console.log("Se paso el id del local: " +this.id_Local)
  }

  aceptar(){    
    //console.log(this.seccion)
    //console.log(this.formSeccion.value)
    this.seccionServicio.createOrUpdate(this.formSeccion.value).subscribe( //Verifica con el id si esta guardado o no
      resultado => {
        //console.log(resultado)
        this.dialogo.close(resultado) //Cuando se cierre tenenmos que interceptarlo en la vista donde actualizara el listado
      }
    )
  }
  cancelar(){
    this.dialogo.close(null)
  }

}
