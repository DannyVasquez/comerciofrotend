import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeccionDialogoComponent } from './seccion-dialogo.component';

describe('SeccionDialogoComponent', () => {
  let component: SeccionDialogoComponent;
  let fixture: ComponentFixture<SeccionDialogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeccionDialogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeccionDialogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
