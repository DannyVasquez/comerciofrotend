import { Component,  OnInit, Input, Output, EventEmitter } from '@angular/core';


import { Seccion } from 'src/app/modelo/comercio/seccion.model';
import { SeccionService } from 'src/app/modelo/comercio/seccion.service';


import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SeccionDialogoComponent } from './seccion-dialogo/seccion-dialogo.component';
import { UtilConfirmacionComponent } from '../util-confirmacion/util-confirmacion.component';

@Component({
  selector: 'app-util-seccion',
  templateUrl: './util-seccion.component.html',
  styleUrls: ['./util-seccion.component.scss']
})
export class UtilSeccionComponent implements OnInit {
 
  //secciones: Seccion[]
  seccion: Seccion
  @Input() seccionesABuscar: number //Retorna el id del local que recorrio en el for de los locales

  seccionesBUSCADAS: Seccion[]

  constructor(private seccionesService: SeccionService,public dialog: MatDialog) { }

  ngOnInit() {
    //this.cargarSecciones()
    //console.log(this.cargarSecciones())
    this.cargarSECCIONESID(this.seccionesABuscar) //se manda el id a otro metodo

  }
  //CONSULTA
  cargarSECCIONESID(id: number){
    this.seccionesService.listarSecciones(id).subscribe(
      resultado3 => { //Devuelve las secciones que contengan el id del local
        this.seccionesBUSCADAS = resultado3
      }
    )
  }  
  

  agregarSeccion() {
    //console.log("NUEVO SECCION")
    this.seccion = new Seccion()
    this.abrirDialogo2(this.seccionesABuscar)//SE PARA EL ID DEL LOCAL
  }


  editarSeccion(seccion) {
    this.seccion = seccion
    this.abrirDialogo2(this.seccionesABuscar)//SE PARA EL ID DEL LOCAL
  }
  abrirDialogo2(idLocal: number) {
    const dialogConfig2 = new MatDialogConfig();
    dialogConfig2.data = {
      seccion: this.seccion,
      id_Local: idLocal //SE MANDA EN LA DATA EL ID DEL LOCAL
    };
    const dialogRef = this.dialog.open(SeccionDialogoComponent, dialogConfig2)
    dialogRef.afterClosed().subscribe(
      resultado => {
        console.log(resultado)
        if (resultado) {
          this.cargarSECCIONESID(this.seccionesABuscar)
        }
      }//Ente metodo se va ejecutar cuando se cierre el dialogo que se ve al presionar el boton de editar
    )
  }


  confirmarDialogo2(seccion: Seccion) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      titulo: "¿Está seguro de eliminar la seccion?"
    };
    const dialogRef = this.dialog.open(UtilConfirmacionComponent, dialogConfig)
    dialogRef.afterClosed().subscribe(
      resultado => {
        if (resultado) {
          this.eliminarSeccion(seccion)
        }
      }
    )
  }


  eliminarSeccion(seccion: Seccion) {
    this.seccionesService.delete(seccion).subscribe(
      resultado => {
        this.cargarSECCIONESID(this.seccionesABuscar)
      }
    )
  }

  


 //consulta Retorna todos las Secciones de la BD 
  /*cargarSecciones() {
    this.seccionesService.list().subscribe(
      resultado2 => {
        this.secciones = resultado2
      }
    )    
  }*/

  
}
