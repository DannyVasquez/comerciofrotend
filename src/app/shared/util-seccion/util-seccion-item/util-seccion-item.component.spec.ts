import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilSeccionItemComponent } from './util-seccion-item.component';

describe('UtilSeccionItemComponent', () => {
  let component: UtilSeccionItemComponent;
  let fixture: ComponentFixture<UtilSeccionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilSeccionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilSeccionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
