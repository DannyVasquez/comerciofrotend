import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Seccion } from 'src/app/modelo/comercio/seccion.model';
@Component({
  selector: 'app-util-seccion-item',
  templateUrl: './util-seccion-item.component.html',
  styleUrls: ['./util-seccion-item.component.scss']
})
export class UtilSeccionItemComponent implements OnInit {
  
 @Input() sec233: Seccion //Recorrera cada seccion en el html de este componente

 @Output() emitirEditar2 = new EventEmitter()
 @Output() emitirEliminar2 = new EventEmitter()

  constructor() { }

  ngOnInit() {
  }

  accionEditarSeccion() {
    this.emitirEditar2.emit(this.sec233)
  }

  accionEliminarSeccion() {
    this.emitirEliminar2.emit(this.sec233)
  }

  
}
